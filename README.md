# Java - Administration d'un Hôpital

Ce projet est une introduction pratique aux principes de l'héritage en Java, à travers un système simplifié pour gérer les patients et le personnel médical d’un hôpital. Le système permet d'enregistrer les nouveaux patients, de stocker des informations médicales, et de gérer les médecins ainsi que les infirmiers.

## Fonctionnalités

- **Enregistrement des Patients** : Permet d'ajouter de nouveaux patients au système avec leurs informations de base et médicales.
- **Gestion du Personnel Médical** : Ajoute, met à jour, et supprime des médecins et des infirmiers du système.
- **Affichage des Informations** : Affiche les détails des patients et du personnel médical, incluant les spécialités des médecins et les affectations des infirmiers.

## Démarrage Rapide

1. **Clonez le dépôt** :
   ```bash
   git clone git@gitlab.com:flojarno/hospital-management.git
    ```
2. Ouvrez le projet dans votre IDE

3. Compilez et exécutez le programme
- Naviguez dans le dossier racine du projet.
- Compilez le fichier source :
   ```bash
  javac Main.java
   ```
- Exécutez le programme compilé :
     ```bash
  java Main
   ```