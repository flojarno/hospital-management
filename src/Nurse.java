public class Nurse extends MedicalStaff {

    public Nurse(String nurseName, int nurseAge, String nurseSSN, String employeeId) {
        super(nurseName, nurseAge, nurseSSN, employeeId);
    }
    @Override
    public void getRole() {
        System.out.println("Nurse");
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Nurse " + super.getName() + " cares for " + patient.getName());
    }
}
