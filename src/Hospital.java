import java.util.ArrayList;

public class Hospital {
    public static void main(String[] args) {

        // Meds
        Medication advil = new Medication("Advil", "500mg");
        Medication doliprane = new Medication("Doliprane", "1000mg");
        Medication tramadol = new Medication("Tramadol", "30mg");

        System.out.println(doliprane.getInfo());
        System.out.println(advil.getInfo());

        // Med lists
        ArrayList<Medication> medListFlue = new ArrayList<>();
        ArrayList<Medication> medListPain = new ArrayList<>();

        // Illness
        Illness flue = new Illness("Flue", medListFlue);
        flue.addMedication(advil);
        flue.addMedication(doliprane);
        System.out.println(flue.getInfo());
        Illness backPain = new Illness("BackPain", medListPain);
        backPain.addMedication(tramadol);
        backPain.getInfo();

        // Illness lists
        ArrayList<Illness> illnessList1 = new ArrayList<>();
        ArrayList<Illness> illnessList2 = new ArrayList<>();

        // Patients
        Patient patient1 = new Patient("Smith", 26, "1234567898745", "pid1", illnessList1);
        Patient patient2 = new Patient("Dupond", 41, "223456789123", "pid2", illnessList2);
        patient1.addIllness(flue);
        patient2.addIllness(backPain);
        System.out.println(patient1.getSocialSecurityNumber());
        System.out.println(patient2.getInfo());

        // Doctor
        Doctor doctor1 = new Doctor("Martin", 51, "147852369852", "eid1", "back");
        doctor1.careForPatient(patient2);
        doctor1.getRole();
        System.out.println(doctor1.getAge());

        // Nurse
        Nurse nurse1 = new Nurse("White", 29, "258963144789", "eid2");
        nurse1.careForPatient(patient1);
        nurse1.recordPatientVisit("Visit from family at 5pm");
        System.out.println(nurse1.getName());
    }
}