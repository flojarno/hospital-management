import java.util.ArrayList;

public class Illness {

    private String name;
    private ArrayList<Medication> medicationList;

    public Illness(String name, ArrayList<Medication> medicationList) {
        this.name = name;
        this.medicationList = medicationList;
    }

    public void addMedication(Medication medication){
        this.medicationList.add(medication);
    }

    public String getInfo() {
        return "Name: " + this.name + ", Medication list: " + this.medicationList;
    }
}

