import java.util.ArrayList;

public class Patient extends Person {

    private String patientid;
    private ArrayList<Illness> illnessList;

    public Patient(String patientName, int patientAge, String patientNumSS, String patientid, ArrayList<Illness> illnessList) {
        super(patientName, patientAge,patientNumSS);
        this.patientid = patientid;
        this.illnessList = illnessList;
    }

    public void addIllness(Illness illness){
        this.illnessList.add(illness);
    }
    // retourne les informations du patient (nom, âge, numéro de sécurité sociale et les informations sur ses maladies)
    public String getInfo(){

        String illnessInfo = "";
        for (Illness illness : this.illnessList){
            illnessInfo = String.join(", ", illness.getInfo());
        }

        String patientInfo =
                "Patient name : " + super.getName() +
                ", Age : " + super.getAge() +
                ", Social security number : " + super.getSocialSecurityNumber() +
                ", Illness : " + illnessInfo;
        return patientInfo;
    }
}
