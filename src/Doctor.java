public class Doctor extends MedicalStaff {

   private String specialty;

    public Doctor(String doctorName, int doctorAge, String doctorSSN, String employeeId, String specialty) {
        super(doctorName, doctorAge, doctorSSN, employeeId);
        this.specialty = specialty;
    }

   @Override
   public void getRole(){
       System.out.println("Doctor");
   }

   @Override
   public void careForPatient(Patient patient) {
       System.out.println("Doctor " + super.getName() + " cares for " + patient.getName());
   }
}
